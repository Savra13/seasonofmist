# SeasonOfMist

## Get Started
### Clone the depot
```
git clone https://Savra13@bitbucket.org/Savra13/seasonofmist.git
```
### Install
```
cd seasonofmist
npm install
```
### Run in development
```
npm start
```
The app is serve by default at [http://localhost:4200](http://localhost:4200/)
### Make a build for Production
```
ng build --prod
```
### Run the Production build
The application has to be served to find all ressources at their correct path. You can install http-serve to quickly serve the app: 
```
npm install -g http-server
```
Then go to the `dist` folder and serve the app :
```
cd dist/season-of-mist
http-server
```
Then go to your favorite browser at [http://127.0.0.1:8080](http://127.0.0.1:8080)
