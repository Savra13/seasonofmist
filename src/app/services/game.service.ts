import {Injectable} from '@angular/core';
import {BehaviorSubject} from 'rxjs';
import {Score} from '../models/score.model';
import {Game} from '../models/game.model';
import {GameStatus} from '../enums/game-status.enum';

@Injectable({
  providedIn: 'root'
})
/**
 * Shares a global state of game status and score
 */
export class GameService {
  private gameList: Game[];

  public scores$: BehaviorSubject<Score[]>;
  private scores: Score[];

  public status$: BehaviorSubject<GameStatus>;

  constructor() {
    this.gameList = this.initGameList();
    this.scores = [];
    this.gameList.forEach((game: Game) => {
      const gameScore = new Score();
      gameScore.game = game;
      gameScore.win = 0;
      gameScore.loose = 0;
      gameScore.totalGame = 0;
      this.scores.push(gameScore);
    });
    this.scores$ = new BehaviorSubject(this.scores);
    this.status$ = new BehaviorSubject(null);
  }

  /**
   * Get all games
   */
  getAllGames() {
    return this.gameList;
  }

  /**
   * Update session score
   * @param game {Game} game to update the score
   * @param win {boolean} Indicate if the game is winning
   */
  updateScores(game: Game, win: boolean) {
    if (game) {
      const scoreToUpdate: Score = this.scores.find((gs: Score) => gs.game.index === game.index);
      if (scoreToUpdate) {
        if (win) {
          scoreToUpdate.win += 1;
        } else {
          scoreToUpdate.loose += 1;
        }
        scoreToUpdate.totalGame += 1;
        this.scores$.next(this.scores);
        this.updateGameStatus(GameStatus.FINISH);
      }
    }
  }

  /**
   * Update the status of the game
   * @param newSatus
   */
  updateGameStatus(newSatus: GameStatus) {
    this.status$.next(newSatus);
  }

  /**
   * Initialise a game list with hardcoded data
   */
  private initGameList() {
    const games = [];
    const game1 = new Game();
    game1.title = 'Anagramme';
    game1.rule = 'Saisissez deux chaîne de caractères. Si ce sont des anagrammes c\'est gagné !';
    game1.index = 1;
    games.push(game1);
    const game2 = new Game();
    game2.title = 'Palindrome';
    game2.rule = 'Saisissez une chaîne de caractères. Si c\'est un palindrome c\'est gagné !';
    game2.index = 2;
    games.push(game2);
    return games;
  }
}
