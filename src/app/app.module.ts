import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { GameComponent } from './components/game/game.component';
import { PalindromeComponent } from './components/palindrome/palindrome.component';
import {ButtonModule, InputTextModule} from 'primeng/primeng';
import {FormsModule} from '@angular/forms';
import { GameEngineComponent } from './components/game-engine/game-engine.component';
import { AnagramComponent } from './components/anagram/anagram.component';
import {GameService} from './services/game.service';
import { ScoreBoardComponent } from './components/score-board/score-board.component';
import {TableModule} from 'primeng/table';

@NgModule({
  declarations: [
    AppComponent,
    GameComponent,
    PalindromeComponent,
    GameEngineComponent,
    AnagramComponent,
    ScoreBoardComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    InputTextModule,
    ButtonModule,
    TableModule
  ],
  providers: [
    GameService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
