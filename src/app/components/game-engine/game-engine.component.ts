import {Component, OnDestroy, OnInit} from '@angular/core';
import {GameService} from '../../services/game.service';
import {Game} from '../../models/game.model';
import {GameStatus} from '../../enums/game-status.enum';
import {Subscription} from 'rxjs';



@Component({
  selector: 'app-game-engine',
  templateUrl: './game-engine.component.html',
  styleUrls: ['./game-engine.component.css']
})
/**
 * Manage games flow
 */
export class GameEngineComponent implements OnInit, OnDestroy {
  private subscriptionToDelete = new Subscription();
  private _gameSelected: Game;
  public gameStatus: GameStatus;
  public gameStatusEnum = GameStatus;
  public counter: number;

  private setInterval: any;

  constructor(private gameService: GameService) {
  }

  ngOnInit() {
    this.subscriptionToDelete.add(
      this.gameService.status$.subscribe(
        (status: GameStatus) => {
          this.gameStatus = status;
          if (status === GameStatus.FINISH) {
            this.automaticReplay();
          }
        }
      )
    );
  }

  ngOnDestroy() {
    this.subscriptionToDelete.unsubscribe();
  }

  get gameSelected(): Game {
    return this._gameSelected;
  }

  set gameSelected(gameSelected: Game) {
    this._gameSelected = gameSelected;
  }

  /**
   * Launch randomly a game
   */
  public launch() {
    clearInterval(this.setInterval);
    this.gameService.updateGameStatus(GameStatus.INITIALISATION);
    // the setTimeout method ensure that the View pass throught all GameStatus states
    setTimeout(() => {
      const gameList: Game[] = this.gameService.getAllGames();
      const randomNumber = Math.floor(Math.random() * gameList.length);
      this.gameSelected = gameList[randomNumber];
      this.gameService.updateGameStatus(GameStatus.IN_PROGRESS);
    }, 0);
  }

  /**
   * Automaticly launch a game after a delay of 5 seconds
    */
  private automaticReplay() {
    this.counter = 5;
    this.setInterval = setInterval(() => {
      this.counter -= 1;
      if (this.counter === 0) {
        this.launch();
      }
    }, 1000);
  }

  public pause() {
    clearInterval(this.setInterval);
  }
}
