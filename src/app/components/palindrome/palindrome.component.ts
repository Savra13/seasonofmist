import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {GameComponent} from '../game/game.component';
import {GameService} from '../../services/game.service';
import {Game} from '../../models/game.model';

@Component({
  selector: 'app-palindrome',
  templateUrl: './palindrome.component.html',
  styleUrls: ['./palindrome.component.css']
})
/**
 * Palindrome is a specific game which extends GameComponent and check if a string reads the same backward as forward
 */
export class PalindromeComponent extends GameComponent implements OnInit {
  @Input() game: Game;
  public inputString: string;

  constructor(gameService: GameService) {
    super(gameService);
  }

  ngOnInit() {
    this.title = this.game.title;
    this.rule = this.game.rule;
  }

  /**
   * Go for game result
   */
  public play() {
    if (this.inputString) {
      this.computeScore(this.game, this.isPalindrome(this.inputString));
    }
  }

  /**
   * Check if the input string is a palindrome
   * @param inputString
   */
  private isPalindrome(inputString: string): boolean {
    const stringToTest: string = this.clearString(inputString);
    for (let i = 0; i < stringToTest.length / 2; i++) {
      if (stringToTest[i] !== stringToTest[stringToTest.length - i - 1]) {
        return false;
      }
    }
    return true;
  }
}
