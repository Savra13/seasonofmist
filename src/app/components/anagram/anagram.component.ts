import {Component, Input, OnInit} from '@angular/core';
import {GameComponent} from '../game/game.component';
import {GameService} from '../../services/game.service';
import {Game} from '../../models/game.model';

@Component({
  selector: 'app-anagram',
  templateUrl: './anagram.component.html',
  styleUrls: ['./anagram.component.css']
})
/**
 * Anagram is a specific game which extends GameComponent and check if 2 string contains the same character occurrence
 */
export class AnagramComponent extends GameComponent implements OnInit {
  @Input() game: Game;

  public inputString1: string;
  public inputString2: string;

  constructor(gameService: GameService) {
    super(gameService);
  }

  ngOnInit() {
    this.title = this.game.title;
    this.rule = this.game.rule;
  }

  /**
   * Go for game result
   */
  public play() {
    if (this.inputString1 && this.inputString2) {
      this.computeScore(this.game, this.isAnagram(this.inputString1, this.inputString2));
    }
  }

  /**
   * Check if 2 string are anagram
   * @param inputString1
   * @param inputString2
   */
  private isAnagram(inputString1: string, inputString2: string): boolean {
    const stringToTest1: string = this.clearString(inputString1);
    const stringToTest2: string = this.clearString(inputString2);

    if (stringToTest1.length !== stringToTest2.length) {
      return false;
    }

    // Count the number of occurrence of each character in input string
    const characterOccurrenceString1 = this.countCharacterOccurence(stringToTest1);
    const characterOccurrenceString2 = this.countCharacterOccurence(stringToTest2);

    // Check if each character has the same number of occurence in each input
    for (const key in characterOccurrenceString1) {
      if (!characterOccurrenceString2.hasOwnProperty(key)) {
        return false;
      } else if (characterOccurrenceString1[key] !== characterOccurrenceString2[key]) {
        return false;
      }
    }
    return true;
  }

  /**
   * Count the number of each character appears in the input string
   * @param inputString {string} Input string to check
   */
  private countCharacterOccurence(inputString: string): object {
    const characterOccurrenceString = {};
    for (const c of inputString) {
      if (characterOccurrenceString.hasOwnProperty(c)) {
        characterOccurrenceString[c] += 1;
      } else {
        characterOccurrenceString[c] = 1;
      }
    }
    return characterOccurrenceString;
  }
}
