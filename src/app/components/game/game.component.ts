import {Component, Input, OnInit} from '@angular/core';
import {GameService} from '../../services/game.service';
import {Game} from '../../models/game.model';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css']
})
/**
 * General game class conataining basic properties and method
 */
export class GameComponent implements OnInit {

  protected _title: string;
  protected _rule: string;
  public gameIsWin: boolean;

  constructor(protected gameService: GameService) {
  }

  ngOnInit() {
    this.gameIsWin = undefined;
  }

  get title(): string {
    return this._title;
  }

  set title(title: string) {
    this._title = title;
  }

  get rule(): string {
    return this._rule;
  }

  set rule(rule: string) {
    this._rule = rule;
  }

  /**
   * Call the method to update score and set the result of the game
   * @param game {Game} game to update the score
   * @param test {boolean} result of specific gamecheck to determine if it is win or not
   */
  protected computeScore(game: Game, test: boolean) {
    if (test) {
      this.gameService.updateScores(game, true);
      this.gameIsWin = true;
    } else {
      this.gameService.updateScores(game, false);
      this.gameIsWin = false;
    }
  }

  /**
   * Remove some charater that could be encouterd in a sentence
   * @param inputString
   */
  protected clearString(inputString: string): string {
    return inputString.replace(/[.,;:?!-\s]/g, '');
  }
}
