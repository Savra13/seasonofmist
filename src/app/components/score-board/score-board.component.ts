import {Component, OnDestroy, OnInit} from '@angular/core';
import {Score} from '../../models/score.model';
import {GameService} from '../../services/game.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-score-board',
  templateUrl: './score-board.component.html',
  styleUrls: ['./score-board.component.css']
})
/**
 * Display the score board
 */
export class ScoreBoardComponent implements OnInit, OnDestroy {
  private subscriptionToDelete = new Subscription();
  public scores: Score[];

  constructor(private gameService: GameService) {
  }

  ngOnInit() {
    this.subscriptionToDelete.add(
      this.gameService.scores$.subscribe(
        (scores: Score[]) => {
          this.scores = scores;
        }
      )
    );
  }

  ngOnDestroy() {
    this.subscriptionToDelete.unsubscribe();
  }
}
