import {Game} from './game.model';

export class Score {
  private _game: Game;
  private _win: number;
  private _loose: number;
  private _totalGame: number;

  get game(): Game {
    return this._game;
  }
  set game(game: Game) {
    this._game = game;
  }

  get win(): number {
    return this._win;
  }
  set win(win: number) {
    this._win = win;
  }

  get loose(): number {
    return this._loose;
  }
  set loose(loose: number) {
    this._loose = loose;
  }

  get totalGame(): number {
    return this._totalGame;
  }
  set totalGame(totalGame: number) {
    this._totalGame = totalGame;
  }
}
