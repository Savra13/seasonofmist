export class Game {
  private _title: string;
  private _rule: string;
  private _index: number;

  get title(): string {
    return this._title;
  }

  set title(title: string) {
    this._title = title;
  }

  get rule(): string {
    return this._rule;
  }

  set rule(rule: string) {
    this._rule = rule;
  }

  get index(): number {
    return this._index;
  }

  set index(index: number) {
    this._index = index;
  }
}
